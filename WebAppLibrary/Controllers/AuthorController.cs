﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebAppLibrary.Models.DAO;
using WebAppLibrary.Models.DataAccess;

namespace WebAppLibrary.Controllers
{
    public class AuthorController : Controller
    {

        private readonly string _connectionString;
        DAAuthor dataAccess;
        public AuthorController(IConfiguration configuration) {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccess = new DAAuthor(_connectionString);
        }

        // GET: AuthorController
        public ActionResult Index()
        {
            List<Author> list = dataAccess.Read(); 
            return View(list);
        }

        // GET: AuthorController/Details/5
        public ActionResult Details(int id)
        {
            Author author = dataAccess.ReadSingle(id);
            return View(author);
        }

        // GET: AuthorController/Create
        public ActionResult Create()
        {
            DACountry dataAccessCountry = new DACountry(_connectionString);
            ViewData["CountriesList"] = new SelectList(dataAccessCountry.Read(), "Id", "Nombre");
            return View();
        }

        // POST: AuthorController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Author author)
        {
            try
            {
                dataAccess.Create(author);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AuthorController/Edit/5
        public ActionResult Edit(int id)
        {
            DACountry dataAccessCountry = new DACountry(_connectionString);
            ViewData["CountriesList"] = new SelectList(dataAccessCountry.Read(), "Id", "Nombre");
            return View(dataAccess.ReadSingle(id));
        }

        // POST: AuthorController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Author author)
        {
            try
            {
                dataAccess.Update(author);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AuthorController/Delete/5
        public ActionResult Delete(int id)
        {
            Author author = dataAccess.ReadSingle(id);
            return View(author);
        }

        // POST: AuthorController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                dataAccess.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
