﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebAppLibrary.Models.DAO;
using WebAppLibrary.Models.DataAccess;

namespace WebAppLibrary.Controllers
{
    public class BookController : Controller
    {

        DABook dataAccess;
        readonly string _connectionString;
        public BookController(IConfiguration configuration) {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccess= new DABook(_connectionString); 
        }

        // GET: BookController
        public ActionResult Index()
        {
            List<Book> lista = dataAccess.Read();
            return View(lista);
        }

        // GET: BookController/Details/5
        public ActionResult Details(int id)
        {
            Book book = dataAccess.ReadSingle(id);
            return View(book);
        }

        // GET: BookController/Create
        public ActionResult Create()
        {
            DAAuthor dataAccessBook = new DAAuthor(_connectionString);
            DAGenre dataAccessGenre = new DAGenre(_connectionString);
            ViewData["GenresList"] = new SelectList(dataAccessGenre.Read(), "Id", "Nombre");
            ViewData["AuthorsList"] = new SelectList(dataAccessBook.Read(), "Id", "FullName");
            return View();
        }

        // POST: BookController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Book book)
        {
            try
            {
                dataAccess.Create(book);    
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: BookController/Edit/5
        public ActionResult Edit(int id)
        {
            DAGenre dataAccessGenre = new DAGenre(_connectionString);
            DAAuthor dataAccessAuthor = new DAAuthor(_connectionString);
            ViewData["GenresList"] = new SelectList(dataAccessGenre.Read(), "Id", "Nombre");
            ViewData["AuthorsList"] = new SelectList(dataAccessAuthor.Read(), "Id", "FullName");
            Book book= dataAccess.ReadSingle(id);
            return View(book);
        }

        // POST: BookController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Book book)
        {
            try
            {
                dataAccess.Update(book);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: BookController/Delete/5
        public ActionResult Delete(int id)
        {
            Book book = dataAccess.ReadSingle(id);
            return View(book);
        }

        // POST: BookController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                dataAccess.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
