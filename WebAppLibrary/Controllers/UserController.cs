﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppLibrary.Models.DataAccess;
using WebAppLibrary.Models.DAO;

namespace WebAppLibrary.Controllers
{
    public class UserController : Controller
    {
        private readonly string _connectionString;
        DAUser dataAccess;

        public UserController(IConfiguration configuration) {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccess = new DAUser(_connectionString);

        }
        // GET: UserController
        public ActionResult Index()
        {
            return View(dataAccess.Read());
        }

        // GET: UserController/Details/5
        public ActionResult Details(int id)
        {
            return View(dataAccess.ReadSingle(id));
        }

        // GET: UserController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User usuario)
        {
            try
            {
                dataAccess.Create(usuario);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserController/Edit/5
        public ActionResult Edit(int id)
        {
            return View(dataAccess.ReadSingle(id));
        }

        // POST: UserController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, User usuario)
        {
            try
            {
                dataAccess.Update(usuario);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserController/Delete/5
        public ActionResult Delete(int id)
        {
            return View(dataAccess.ReadSingle(id));
        }

        // POST: UserController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                dataAccess.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
