﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppLibrary.Models.DataAccess;
using WebAppLibrary.Models.DAO;

namespace WebAppLibrary.Controllers
{
    public class CountryController : Controller
    {
        DACountry dataAccess;
        private readonly string _connectionString;
        public CountryController(IConfiguration configuration) {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccess = new DACountry(_connectionString);
        }


        // GET: CountryController
        public ActionResult Index()
        {
            List<Country> list = dataAccess.Read();
            return View(list);
        }

        // GET: CountryController/Details/5
        public ActionResult Details(int id)
        {
            Country country = dataAccess.ReadSingle(id);
            return View(country);
        }

        // GET: CountryController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CountryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Country country)
        {
            try
            {
                dataAccess.Create(country);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CountryController/Edit/5
        public ActionResult Edit(int id)
        {
            Country country = dataAccess.ReadSingle(id);
            return View(country);
        }

        // POST: CountryController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Country country)
        {
            try
            {
                dataAccess.Update(country);
                return RedirectToAction(nameof(Index));
            } 
            catch(Exception e)
            {
                return View();
            }
        }

        // GET: CountryController/Delete/5
        public ActionResult Delete(int id)
        {
            return View(dataAccess.ReadSingle(id));
        }

        // POST: CountryController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                dataAccess.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
