﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebAppLibrary.Models.DAO;
using WebAppLibrary.Models.DataAccess;

namespace WebAppLibrary.Controllers
{
    public class LoanController : Controller
    {
        DALoan dataAccessLoan;
        private readonly string _connectionString;
        public LoanController(IConfiguration configuration) {
            _connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            dataAccessLoan = new DALoan(_connectionString);
        }

        // GET: LoanController
        public ActionResult Index()
        {
            List<Loan> loans = dataAccessLoan.Read();
            return View(loans);
        }

        // GET: LoanController/Details/5
        public ActionResult Details(int id)
        {
            Loan loan = dataAccessLoan.ReadSingle(id);
            return View(loan);
        }

        // GET: LoanController/Create
        public ActionResult Create()
        {
            DAUser dataAccessUser = new DAUser(_connectionString);
            DABook dataAccessBook = new DABook(_connectionString);
            ViewData["UsersList"] = new SelectList(dataAccessUser.Read(), "Id", "FullName");
            ViewData["BooksList"] = new SelectList(dataAccessBook.Read(), "Id", "Title");
            return View();
        }

        // POST: LoanController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Loan loan)
        {
            try
            {
                dataAccessLoan.Create(loan);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LoanController/Edit/5
        public ActionResult Edit(int id)
        {
            DAUser dataAccessUser = new DAUser(_connectionString);
            DABook dataAccessBook = new DABook(_connectionString);
            ViewData["UsersList"] = new SelectList(dataAccessUser.Read(), "Id", "FullName");
            ViewData["BooksList"] = new SelectList(dataAccessBook.Read(), "Id", "Title");
            Loan loan = dataAccessLoan.ReadSingle(id);
            return View(loan);
        }

        // POST: LoanController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Loan loan)
        {
            try
            {
                dataAccessLoan.Update(loan);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LoanController/Delete/5
        public ActionResult Delete(int id)
        {
            Loan loan = dataAccessLoan.ReadSingle(id);
            return View(loan);
        }

        // POST: LoanController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                dataAccessLoan.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
