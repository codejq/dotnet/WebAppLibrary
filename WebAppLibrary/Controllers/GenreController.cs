﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Globalization;
using WebAppLibrary.Models.DataAccess;
using WebAppLibrary.Models.DAO;

namespace WebAppLibrary.Controllers
{
    public class GenreController : Controller
    {

        DAGenre daGenre;

        public GenreController(IConfiguration configuration)
        {
            string connectionString = configuration.GetConnectionString("DefaultDB") ?? "";
            daGenre = new DAGenre(connectionString);
        }

        // GET: GenreController
        public ActionResult Index()
        {
            List<Genre> genres = daGenre.Read();
            return View(genres);
        }

        // GET: GenreController/Details/5
        public ActionResult Details(int id)
        {
            Genre genre = daGenre.ReadSingle(id);
            return View(genre);
        }

        // GET: GenreController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GenreController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Nombre")] Genre genre)
        {
            try
            {
                daGenre.Create(genre);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: GenreController/Edit/5
        public ActionResult Edit(int id)
        {
            return View(daGenre.ReadSingle(id));
        }

        // POST: GenreController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, Genre genre)
        {
            try
            {
                daGenre.Update(genre);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: GenreController/Delete/5
        public ActionResult Delete(int id)
        {
            return View(daGenre.ReadSingle(id));
        }

        // POST: GenreController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                daGenre.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
