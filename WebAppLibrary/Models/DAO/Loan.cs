﻿namespace WebAppLibrary.Models.DAO
{
    public class Loan
    {
        public int Id { get; set; }
        public DateOnly LoanDate { get; set; }
        public DateOnly ReturnDate { get; set; }

        public User User { get; set; }
        public Book Book { get; set; }

    }
}
