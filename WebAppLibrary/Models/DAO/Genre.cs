﻿namespace WebAppLibrary.Models.DAO
{
    public class Genre
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
