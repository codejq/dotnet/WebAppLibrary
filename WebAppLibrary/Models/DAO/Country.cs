﻿namespace WebAppLibrary.Models.DAO
{
    public class Country
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string CodigoArea { get; set; }
        public string Iso2 { get; set; }
    }
}
