﻿using Microsoft.Data.SqlClient;
using System.Data;
using WebAppLibrary.Models.DAO;

namespace WebAppLibrary.Models.DataAccess
{
    public class DACountry
    {
        private readonly string _connectionString;
        public DACountry(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<Country> Read()
        {
            try
            {
                List<Country> list = new List<Country>();
                using(var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "SELECT id_country, name, area_code, iso_3166_1_alpha_2 FROM TBL_COUNTRIES";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using(var reader = command.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                Country country = new Country();
                                country.Id = reader.GetInt32("id_country");
                                country.Nombre = reader.GetString("name");
                                country.CodigoArea = reader.GetString("area_code");
                                country.Iso2 = reader.GetString("iso_3166_1_alpha_2");
                                list.Add(country);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public  Country ReadSingle(int id)
        {
            try
            {
                Country country = new Country();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "SELECT id_country, name, area_code, iso_3166_1_alpha_2 FROM TBL_COUNTRIES WHERE id_country = @id_country";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_country", id);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                country.Id = reader.GetInt32("id_country");
                                country.Nombre = reader.GetString("name");
                                country.CodigoArea = reader.GetString("area_code");
                                country.Iso2 = reader.GetString("iso_3166_1_alpha_2");
                            }
                            return country;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public bool Create(Country country)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO TBL_COUNTRIES(name, area_code, iso_3166_1_alpha_2) VALUES (@name, @area_code, @iso_3166_1_alpha_2)";
                    using(var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@name", country.Nombre);
                        command.Parameters.AddWithValue("@area_code", country.CodigoArea);
                        command.Parameters.AddWithValue("@iso_3166_1_alpha_2", country.Iso2);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch {
                throw;
            }
        }


        public bool Update(Country country)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "UPDATE TBL_COUNTRIES SET name=@name, area_code=@area_code, iso_3166_1_alpha_2=@iso_3166_1_alpha_2 WHERE id_country = @id_country";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_country", country.Id);
                        command.Parameters.AddWithValue("@name", country.Nombre);
                        command.Parameters.AddWithValue("@area_code", country.CodigoArea);
                        command.Parameters.AddWithValue("@iso_3166_1_alpha_2", country.Iso2);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public bool Delete(int id)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM TBL_COUNTRIES WHERE id_country=@id_country";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_country", id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
