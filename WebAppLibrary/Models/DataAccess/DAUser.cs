﻿using Microsoft.Data.SqlClient;
using System.Data;
using WebAppLibrary.Models.DAO;

namespace WebAppLibrary.Models.DataAccess
{
    public class DAUser
    {
        private readonly string _connectionString;
        public DAUser(string connectionString) {
            _connectionString = connectionString;
        }


        public List<User> Read()
        {
            try
            {
                List<User> list = new List<User>();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "SELECT id_user,firstname,lastname,address,email,phone_number FROM TBL_USERS";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                User usuario = new User();
                                usuario.Id = reader.GetInt32("id_user");
                                usuario.FirstName = reader.GetString("firstname");
                                usuario.LastName = reader.GetString("lastname");
                                usuario.Email = reader.GetString("email");
                                usuario.Address = reader.GetString("address");
                                usuario.PhoneNumber = reader.GetString("phone_number");
                                list.Add(usuario);
                            }
                            return list;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public User ReadSingle(int id)
        {
            try
            {
                User usuario = new User();
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "SELECT id_user,firstname,lastname,address,email,phone_number FROM TBL_USERS WHERE id_user = @id_user";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_user", id);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                usuario.Id = reader.GetInt32("id_user");
                                usuario.FirstName = reader.GetString("firstname");
                                usuario.LastName = reader.GetString("lastname");
                                usuario.Email = reader.GetString("email");
                                usuario.Address = reader.GetString("address");
                                usuario.PhoneNumber = reader.GetString("phone_number");
                            }
                            return usuario;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public bool Create(User usuario)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "INSERT INTO TBL_USERS(firstname,lastname,address,email,phone_number) VALUES (@firstname,@lastname,@address,@email,@phone_number)";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@firstname", usuario.FirstName);
                        command.Parameters.AddWithValue("@lastname", usuario.LastName);
                        command.Parameters.AddWithValue("@address", usuario.Address);
                        command.Parameters.AddWithValue("@email", usuario.Email);
                        command.Parameters.AddWithValue("@phone_number", usuario.PhoneNumber);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public bool Update(User usuario)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "UPDATE TBL_USERS SET firstname=@firstname,lastname=@lastname,address=@address,email=@email,phone_number=@phone_number WHERE id_user = @id_user";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_user", usuario.Id);
                        command.Parameters.AddWithValue("@firstname", usuario.FirstName);
                        command.Parameters.AddWithValue("@lastname", usuario.LastName);
                        command.Parameters.AddWithValue("@address", usuario.Address);
                        command.Parameters.AddWithValue("@email", usuario.Email);
                        command.Parameters.AddWithValue("@phone_number", usuario.PhoneNumber);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public bool Delete(int id)
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    string sql = "DELETE FROM TBL_USERS WHERE id_user=@id_user";
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_user", id);
                        int rowsAffected = command.ExecuteNonQuery();
                        return rowsAffected > 0;
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
