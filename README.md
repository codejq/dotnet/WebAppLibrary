# Library Management

## Description

This project is a Library Management System developed as an ASP.NET Core Web App (MVC) using Microsoft Visual Studio Community. It allows users to borrow and return books, manage authors, genres, countries, and users.

## Features

- ASP.NET Core Web App (MVC)
- Developed in Microsoft Visual Studio Community
- Utilizes .NET 7
- Database: Microsoft SQL Server
- Entity models: Author, Book, Country, Genre, Loan, User
- Data access classes in the Models folder
- Database connection: C# with SQLClient 5.2

## Requirements

- Microsoft Visual Studio Community
- .NET 7
- Microsoft SQL Server

## Installation and Setup

1. Clone the repository from GitHub:

    ```bash
    git clone https://gitlab.com/codejq/dotnet/WebAppLibrary.git
    ```

2. Open the project in Microsoft Visual Studio Community.

3. Configure the database connection string in the `appsettings.json` file.

4. Run SQL scripts to create a database of libraries, which are located in the scripts folder.

5. Run the project.

## Usage

- Access the application from your web browser.
- Explore the different functionalities to manage books, authors, loans and catalogs.

## Contribution

Contributions are welcome. If you wish to contribute to the project, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature: `git checkout -b feature-new-feature`.
3. Make your changes and commit them: `git commit -m 'Add new feature'`.
4. Push your branch: `git push origin feature-new-feature`.
5. Create a pull request on Gitlab.

## License

This project is licensed under the MIT License.
